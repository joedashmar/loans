# Loans Application
Hi! My name is Joe-mar Chavez. This repository is my submission for the tech task from MoneyMe. You can also find the debug apk build (app-debug.apk) in this repository to try it out. Please reach out to me if you are having problems. Thank you!

# MVVM Architecture
This project uses the MVVM architecture. I used MVVM since this architecture enhances separation of concerns. I utilized the ViewModel for the communication of the activity and the fragments.

# Tools
## Kotlin
This project is now in Kotlin. Aside from Kotlin being the preferred language now by Google for developing Android apps, we can utilize its strength like writability and many more!

## Google Sign In
For the third party authenticator, I used Google Sign In. You can get the name and email from the account that the user selected.

## Room
For storing the user account and loan application, I used Room from the Jetpack. It makes managing of databases easier.

## AndroidX Library
Basically this is the new version of the Android Support Library. It is recommended to use this for new projects and also migrate from existing ones.

## Material Components
Same reason and purpose with the use of AndroidX. Utilizing and keeping up to date with the components.
