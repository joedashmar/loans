package com.loans

import androidx.room.Database
import androidx.room.RoomDatabase

@Database(entities = [User::class, LoanApplication::class], version = 1, exportSchema = false)
abstract class AppDatabase : RoomDatabase() {
    abstract fun userDao(): UserDao
    abstract fun loanApplicationDao(): LoanApplicationDao
}
