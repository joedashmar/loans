package com.loans

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import com.warkiz.widget.IndicatorSeekBar
import com.warkiz.widget.OnSeekChangeListener
import com.warkiz.widget.SeekParams
import kotlinx.android.synthetic.main.calculator_fragment.*
import java.text.NumberFormat
import java.util.*

class CalculatorFragment : Fragment() {

    companion object {
        fun newInstance() = CalculatorFragment()
        const val INITIAL_AMOUNT = 2000
        const val INITIAL_MONTHS = 3
    }

    private lateinit var viewModel: MainViewModel
    private lateinit var amountIndicatorTextView: TextView

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.calculator_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        activity?.let {
            viewModel = ViewModelProviders.of(it).get(MainViewModel::class.java)
            viewModel.amount.postValue(INITIAL_AMOUNT)
            viewModel.months.postValue(INITIAL_MONTHS)
        }

        setupViews()
    }

    private fun setupViews() {
        val horizontalPadding = resources.getDimension(R.dimen.indicator_horizontal_padding).toInt()
        val verticalPadding = resources.getDimension(R.dimen.indicator_vertical_padding).toInt()

        amountIndicatorTextView = TextView(activity)
        amountIndicatorTextView.setTextColor(ContextCompat.getColor(activity!!, R.color.white))
        amountIndicatorTextView.textAlignment = View.TEXT_ALIGNMENT_CENTER
        amountIndicatorTextView.setPadding(horizontalPadding, verticalPadding, horizontalPadding, verticalPadding)
        amountIndicatorTextView.text = formatAmountIndicator(amountSeekBar.progress)

        amountSeekBar.indicator.topContentView = amountIndicatorTextView
        amountSeekBar.onSeekChangeListener = object : OnSeekChangeListener {
            override fun onSeeking(seekParams: SeekParams?) {
                val progress = seekParams?.progress!!.toInt()

                amountIndicatorTextView.text = formatAmountIndicator(progress)
                viewModel.amount.postValue(computeAmount(progress))
            }

            override fun onStartTrackingTouch(seekBar: IndicatorSeekBar?) {
            }

            override fun onStopTrackingTouch(seekBar: IndicatorSeekBar?) {
            }
        }

        monthsSeekBar.setIndicatorTextFormat("\${PROGRESS} months")
        monthsSeekBar.onSeekChangeListener = object : OnSeekChangeListener {
            override fun onSeeking(seekParams: SeekParams?) {
                val months = seekParams?.progress!!.toInt()

                viewModel.months.postValue(months)
            }

            override fun onStartTrackingTouch(seekBar: IndicatorSeekBar?) {
            }

            override fun onStopTrackingTouch(seekBar: IndicatorSeekBar?) {
            }
        }

        calculateQuoteButton.setOnClickListener {
            val amount = computeAmount(amountSeekBar.progress)
            val monthlyRepayments = viewModel.calculateMonthlyRepayments(amount, monthsSeekBar.progress)

            viewModel.monthlyRepayments.postValue(monthlyRepayments)
            (activity as MainActivity).onCalculateQuote()
        }
    }

    private fun formatAmountIndicator(progress: Int): String {
        return "$" + NumberFormat.getNumberInstance(Locale.US).format(computeAmount(progress))
    }

    private fun computeAmount(progress: Int): Int {
        return 2000 + (progress * 50)
    }
}
