package com.loans

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class LoanApplication(
    @ColumnInfo(name = "amount") val amount: String?,
    @ColumnInfo(name = "months") val mounts: String?,
    @ColumnInfo(name = "monthly_repayments") val monthlyRepayments: String
) {
    @PrimaryKey(autoGenerate = true)
    var uid: Int = 0
}
