package com.loans

import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.Query

@Dao
interface LoanApplicationDao {
    @Query("SELECT * FROM loanApplication")
    fun getAll(): List<LoanApplication>

    @Query("SELECT * FROM loanApplication WHERE uid IN (:loanApplicationIds)")
    fun loadAllByIds(loanApplicationIds: IntArray): List<LoanApplication>

    @Query(
        "SELECT * FROM loanApplication WHERE amount LIKE :amount AND " +
                "months LIKE :months AND monthly_repayments LIKE :monthlyRepayments LIMIT 1"
    )
    fun findByAmountMonthsMonthlyRepayments(amount: String, months: String, monthlyRepayments: String): LoanApplication

    @Insert
    fun insertAll(vararg loanApplications: LoanApplication)

    @Delete
    fun delete(loanApplication: LoanApplication)
}
