package com.loans

import android.content.Context
import android.os.AsyncTask
import androidx.room.Room

class LoansRepository(context: Context) {

    private val database: AppDatabase

    companion object {
        const val DB_NAME = "app-database"
    }

    val users: List<User>
        get() = database.userDao().getAll()

    init {
        database = Room.databaseBuilder(
            context,
            AppDatabase::class.java, DB_NAME
        ).build()
    }

    fun insertUser(name: String, mobile: String, email: String) {
        val user = User(name, mobile, email)

        object : AsyncTask<Void, Void, Void>() {
            override fun doInBackground(vararg voids: Void): Void? {
                database.userDao().insertAll(user)
                return null
            }
        }.execute()
    }

    fun insertLoanApplication(amount: String, months: String, monthlyRepayments: String) {
        val loanApplication = LoanApplication(amount, months, monthlyRepayments)

        object : AsyncTask<Void, Void, Void>() {
            override fun doInBackground(vararg voids: Void): Void? {
                database.loanApplicationDao().insertAll(loanApplication)
                return null
            }
        }.execute()
    }
}
