package com.loans

import android.graphics.Point
import android.os.Bundle
import android.view.*
import androidx.fragment.app.DialogFragment
import androidx.lifecycle.ViewModelProviders
import kotlinx.android.synthetic.main.login_dialog_fragment.*

class LoginDialogFragment : DialogFragment() {

    companion object {
        fun newInstance() = LoginDialogFragment()
    }

    private lateinit var viewModel: MainViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.login_dialog_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        activity?.let {
            viewModel = ViewModelProviders.of(it).get(MainViewModel::class.java)
        }

        setupViews()
    }

    private fun setupViews() {
        startApplicationButton.setOnClickListener {
            (activity as MainActivity).replaceToSummaryFragmentAndAddToBackStack()
            dismiss()
        }

        membersLoginButton.setOnClickListener {
            (activity as MainActivity).googleSignIn()
            dismiss()
        }
    }

    override fun onResume() {
        super.onResume()

        setDialogWidth(0.9)
    }

    private fun setDialogWidth(screenPercentage: Double) {
        val window = dialog.window
        val size = Point()

        val display = window?.windowManager?.defaultDisplay
        display?.getSize(size)

        val width = size.x

        window?.setLayout((width * screenPercentage).toInt(), WindowManager.LayoutParams.WRAP_CONTENT)
        window?.setGravity(Gravity.CENTER)
    }
}
