package com.loans

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProviders
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import com.google.android.gms.auth.api.signin.GoogleSignInClient
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.common.api.ApiException
import com.google.android.gms.tasks.Task


class MainActivity : AppCompatActivity() {

    private lateinit var viewModel: MainViewModel
    private lateinit var calculatorFragment: CalculatorFragment
    private lateinit var summaryFragment: SummaryFragment
    private lateinit var loansRepository: LoansRepository
    private lateinit var googleSignInClient: GoogleSignInClient

    companion object {
        const val RC_SIGN_IN = 0
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        loansRepository = LoansRepository(this)

        viewModel = ViewModelProviders.of(this).get(MainViewModel::class.java)
        calculatorFragment = CalculatorFragment.newInstance()

        val fragment = supportFragmentManager.findFragmentById(R.id.fragment_container)

        if (fragment == null) {
            addCalculatorFragment()
        }

        val gso = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
            .requestEmail()
            .requestProfile()
            .requestId()
            .build()

        googleSignInClient = GoogleSignIn.getClient(this, gso)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == RC_SIGN_IN) {
            val task = GoogleSignIn.getSignedInAccountFromIntent(data)
            handleSignInResult(task)
        }
    }

    private fun handleSignInResult(completedTask: Task<GoogleSignInAccount>) {
        try {
            val account = completedTask.getResult(ApiException::class.java)

            viewModel.name.value = account?.displayName
            viewModel.email.value = account?.email
            replaceToSummaryFragmentAndAddToBackStack()
        } catch (e: ApiException) {
        }
    }

    private fun addCalculatorFragment() {
        val transaction = supportFragmentManager.beginTransaction()

        transaction.add(R.id.fragment_container, calculatorFragment)
        transaction.commit()
    }

    fun onCalculateQuote() {
        if (viewModel.hasLoggedIn) {
            if (supportFragmentManager.backStackEntryCount == 0) {
                replaceToSummaryFragmentAndAddToBackStack()
            } else {
                popBackStackToSummaryFragment()
            }
        } else {
            showLoginDialogFragment()
        }
    }

    private fun popBackStackToSummaryFragment() {
        supportFragmentManager.popBackStack()
    }

    private fun showLoginDialogFragment() {
        val dialogFragment = LoginDialogFragment.newInstance()

        dialogFragment.show(supportFragmentManager, "dialog")
    }

    fun replaceToSummaryFragmentAndAddToBackStack() {
        viewModel.hasLoggedIn = true
        if (!::summaryFragment.isInitialized) {
            summaryFragment = SummaryFragment.newInstance()
        }
        val transaction = supportFragmentManager.beginTransaction()

        transaction.replace(R.id.fragment_container, summaryFragment)
        transaction.addToBackStack(null)
        transaction.commit()
    }

    fun replaceToCalculatorFragmentAndAddToBackStack() {
        val transaction = supportFragmentManager.beginTransaction()

        transaction.replace(R.id.fragment_container, calculatorFragment)
        transaction.addToBackStack(null)
        transaction.commit()
    }

    fun insertUser(name: String, mobile: String, email: String) {
        loansRepository.insertUser(name, mobile, email)
    }

    fun insertLoanApplication(amount: String, months: String, monthlyRepayments: String) {
        loansRepository.insertLoanApplication(amount, months, monthlyRepayments)
    }

    fun showSuccessfulLoanApplication() {
        AlertDialog.Builder(this).setMessage(getString(R.string.your_application_is_successful))
            .setPositiveButton(
                getString(R.string.okay)
            ) { dialog, _ -> dialog.dismiss() }
            .show()
    }

    fun googleSignIn() {
        val signInIntent = googleSignInClient.signInIntent
        startActivityForResult(signInIntent, RC_SIGN_IN)
    }
}
