package com.loans

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel;
import kotlin.math.pow
import kotlin.math.roundToLong

class MainViewModel : ViewModel() {

    val name = MutableLiveData<String>()
    val mobile = MutableLiveData<String>()
    val email = MutableLiveData<String>()
    val monthlyRepayments = MutableLiveData<Double>()
    val amount = MutableLiveData<Int>()
    val months = MutableLiveData<Int>()

    var hasLoggedIn = false
    var isEditingInfo = false

    companion object {
        const val INTEREST_RATE = 0.0899 / 12
    }

    fun calculateMonthlyRepayments(amount: Int, months: Int): Double {
        val pmt = (amount * INTEREST_RATE) / (1 - (1 + INTEREST_RATE).pow(-1 * months))

        return (pmt * 100).roundToLong().toDouble() / 100
    }
}
