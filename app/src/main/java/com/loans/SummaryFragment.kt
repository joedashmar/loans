package com.loans

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import kotlinx.android.synthetic.main.summary_fragment.*

class SummaryFragment : Fragment() {

    companion object {
        fun newInstance() = SummaryFragment()
    }

    private lateinit var viewModel: MainViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.summary_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        activity?.let {
            viewModel = ViewModelProviders.of(it).get(MainViewModel::class.java)
        }

        setupViews()
    }

    override fun onResume() {
        super.onResume()

        nameText.text = viewModel.name.value?.toString()
        mobileText.text = viewModel.mobile.value?.toString()
        emailText.text = viewModel.email.value?.toString()
        financeAmountText.text = getString(R.string.loan_amount, viewModel.amount.value.toString())
        financeMonthsText.text = getString(R.string.loan_period, viewModel.months.value.toString())
        repaymentsText.text = getString(R.string.monthly_repayments, viewModel.monthlyRepayments.value.toString())
    }

    private fun setupViews() {
        setInfoViews(viewModel.isEditingInfo)

        editInformationButton.setOnClickListener {
            val imm = context?.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager

            imm.hideSoftInputFromWindow(nameEditText.windowToken, 0)
            imm.hideSoftInputFromWindow(mobileEditText.windowToken, 0)
            imm.hideSoftInputFromWindow(emailEditText.windowToken, 0)

            toggleInfoButton()
            toggleName()
            toggleMobile()
            toggleEmail()
        }

        editFinanceButton.setOnClickListener {
            (activity as MainActivity).replaceToCalculatorFragmentAndAddToBackStack()
        }

        applyNowButton.setOnClickListener {
            (activity as MainActivity).insertUser(
                nameText.text.toString(),
                mobileText.text.toString(),
                emailText.text.toString()
            )
            (activity as MainActivity).insertLoanApplication(
                financeAmountText.text.toString(),
                financeMonthsText.text.toString(),
                repaymentsMonthlyText.text.toString()
            )
            (activity as MainActivity).showSuccessfulLoanApplication()
        }
    }

    private fun setInfoViews(isEditInfo: Boolean) {
        if (isEditInfo) {
            editInformationButton.text = resources.getText(R.string.save)
            nameText.visibility = View.INVISIBLE
            nameEditText.visibility = View.VISIBLE
            mobileText.visibility = View.INVISIBLE
            mobileEditText.visibility = View.VISIBLE
            emailText.visibility = View.INVISIBLE
            emailEditText.visibility = View.VISIBLE
        } else {
            editInformationButton.text = resources.getText(R.string.edit)
            nameText.visibility = View.VISIBLE
            nameEditText.visibility = View.INVISIBLE
            mobileText.visibility = View.VISIBLE
            mobileEditText.visibility = View.INVISIBLE
            emailText.visibility = View.VISIBLE
            emailEditText.visibility = View.INVISIBLE
        }
    }

    private fun toggleInfoButton() {
        viewModel.isEditingInfo = !viewModel.isEditingInfo
        val infoText = editInformationButton.text.toString().toLowerCase()

        if (infoText == "edit") {
            editInformationButton.text = resources.getText(R.string.save)
        } else if (infoText == "save") {
            editInformationButton.text = resources.getText(R.string.edit)
        }
    }

    private fun toggleName() {
        if (nameText.visibility == View.VISIBLE) {
            nameText.visibility = View.INVISIBLE
        } else if (nameText.visibility == View.INVISIBLE) {
            nameText.visibility = View.VISIBLE
        }

        if (nameEditText.visibility == View.VISIBLE) {
            nameEditText.visibility = View.INVISIBLE
        } else if (nameEditText.visibility == View.INVISIBLE) {
            nameEditText.visibility = View.VISIBLE
        }

        if (viewModel.isEditingInfo) {
            nameEditText.setText(nameText.text)
        } else {
            nameText.text = nameEditText.text
        }
    }

    private fun toggleMobile() {
        if (mobileText.visibility == View.VISIBLE) {
            mobileText.visibility = View.INVISIBLE
        } else if (mobileText.visibility == View.INVISIBLE) {
            mobileText.visibility = View.VISIBLE
        }

        if (mobileEditText.visibility == View.VISIBLE) {
            mobileEditText.visibility = View.INVISIBLE
        } else if (mobileEditText.visibility == View.INVISIBLE) {
            mobileEditText.visibility = View.VISIBLE
        }

        if (viewModel.isEditingInfo) {
            mobileEditText.setText(mobileText.text)
        } else {
            mobileText.text = mobileEditText.text
        }
    }

    private fun toggleEmail() {
        if (emailText.visibility == View.VISIBLE) {
            emailText.visibility = View.INVISIBLE
        } else if (emailText.visibility == View.INVISIBLE) {
            emailText.visibility = View.VISIBLE
        }

        if (emailEditText.visibility == View.VISIBLE) {
            emailEditText.visibility = View.INVISIBLE
        } else if (emailEditText.visibility == View.INVISIBLE) {
            emailEditText.visibility = View.VISIBLE
        }

        if (viewModel.isEditingInfo) {
            emailEditText.setText(emailText.text)
        } else {
            emailText.text = emailEditText.text
        }
    }
}
