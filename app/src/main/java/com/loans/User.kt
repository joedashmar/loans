package com.loans

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class User(
    @ColumnInfo(name = "name") val name: String?,
    @ColumnInfo(name = "mobile") val mobile: String?,
    @ColumnInfo(name = "email") val email: String
) {
    @PrimaryKey(autoGenerate = true)
    var uid: Int = 0
}
