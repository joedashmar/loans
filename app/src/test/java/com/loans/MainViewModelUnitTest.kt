package com.loans

import org.junit.Assert.assertEquals
import org.junit.Test

class MainViewModelUnitTest {

    private val viewModel = MainViewModel()

    @Test
    fun calculateMonthlyRepayments_isCorrect() {
        val monthlyRepayments = viewModel.calculateMonthlyRepayments(2000, 3)

        assertEquals(676.68, monthlyRepayments, 0.0)
    }
}
